<!--
.. title: Init
.. slug: init
.. date: 2020-09-27 18:27:57 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Hace un par de años se me cruzo por la cabeza empezar a escribir un blog, genere el repo y la configuración en gitlab y ahí quedo.

Hoy en contexto de pandemia y de aislamiento social se me ocurre retomarlo y acá estoy escribiendo.

Las motivaciones que tengo para hacerlo, son:

* Tener un lugar para guardar notas y no perderlas
* Compartir conocimiento
* Ejercitar la escritura
* Practicar ingles
* Un hobbie
* Empezar a usar Markdown, el formato(?) con el que estoy escribiendo ahora

Así que bueno voy terminando este primer post, a modo de manifiesto, vamos a ver como sigo!

Saludos!(si es que hay alguien leyendo...)
